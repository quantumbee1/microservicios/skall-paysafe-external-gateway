
package com.skall.external.gateway.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.skall.paysafe.security.component.JwtTokenUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

    private final JwtTokenUtil jwtConfig;

    public JwtTokenAuthenticationFilter(JwtTokenUtil jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    private static final Logger logger =
            LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
            FilterChain chain) throws ServletException, IOException {
        logger.info("{} init -> HttpServletRequest[{}] - HttpServletResponse[{}] - FilterChain[{}]",
                request, response, chain);
        // 1. Obtenga el encabezado de autenticación. Se supone que los tokens se pasan en el
        // encabezado de autenticación
        String header = request.getHeader(jwtConfig.getHeader());
        logger.info("{} init -> jwtConfig-header[{}]", header);

        // 2. validar el encabezado y verificar el prefijo
        if (header == null || !header.startsWith(jwtConfig.getPrefix())) {
            chain.doFilter(request, response);
            return;
        }
        // Si no se proporciona ningún token y, por lo tanto, el usuario no se autenticará.
        // Está bien. Tal vez el usuario accede a una ruta pública o solicita un token.

        // Todas las rutas protegidas que necesitan un token ya están definidas y protegidas en la
        // clase de configuración.
        // Y si el usuario intentó acceder sin token de acceso, entonces no será autenticado y un
        // se lanzará una excepción.

        // 3. Obtén el token
        String token = header.replace(jwtConfig.getPrefix(), "");
        logger.info("{} Token -> getToken[{}]", token);
        try {// se pueden generar excepciones al crear las notificaciones si, por ejemplo, el token
             // es Caducado

            // 4. Validar el token
            Claims claims = Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes())
                    .parseClaimsJws(token).getBody();
            String username = claims.getSubject();

            // 5. Crear objeto de autenticación
            // UsernamePasswordAuthenticationToken: un objeto incorporado, utilizado por spring
            // para representa el usuario autenticado / autenticado actual.
            // Necesita una lista de autoridades, que tiene un tipo de interfaz GrantedAuthority,
            // donde SimpleGrantedAuthority es una implementación de esa interfaz

            if (username != null) {
                @SuppressWarnings("unchecked")
                List<String> authorities = (List<String>) claims.get("authorities");
                UsernamePasswordAuthenticationToken auth =
                        new UsernamePasswordAuthenticationToken(username, null, authorities.stream()
                                .map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                // 6. Autenticar al usuario
                // Ahora, el usuario está autenticado
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        } catch (Exception e) {
            // En caso de fallo. Asegúrese de que esté claro; así que garantiza que el usuario no
            // será autenticado
            SecurityContextHolder.clearContext();
        }
        // ir al siguiente filtro en la cadena de filtros
        logger.info("{} ok -> chain.dofilter[{}]", request, response);
        chain.doFilter(request, response);
    }

}
