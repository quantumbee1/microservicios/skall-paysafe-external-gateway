
package com.skall.external.gateway.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.skall.external.gateway.config.ConfigExceptionsProperties;
import com.skall.paysafe.security.component.JwtTokenUtil;

@EnableWebSecurity // Habilitar configuración de seguridad. Esta anotación denota configuración para
                   // la seguridad de primavera.
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtTokenUtil jwtConfig;
    @Autowired
    private ConfigExceptionsProperties configExceptionProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                // asegúrese de usar sesión sin estado; La sesión no se utilizará para almacenar el
                // estado del usuario.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // manejar un intento autorizado
                .exceptionHandling()
                .authenticationEntryPoint(
                        (req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                // Agregue un filtro para validar los tokens con cada solicitud
                .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig),
                        UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests().antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()
                .antMatchers(configExceptionProperties.getExceptions()).permitAll()
                .antMatchers("/skall/client" + "/auth/test").hasRole("ADMIN").anyRequest()
                .authenticated();
    }

    @Bean
    public JwtTokenUtil jwtConfig() {
        return new JwtTokenUtil();
    }
}
